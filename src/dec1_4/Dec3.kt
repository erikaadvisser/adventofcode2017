package dec1_4

//import java.lang.Math.abs
//import java.lang.RuntimeException
//
//fun dec1_4.main(args: Array<String>) {
//
//    val spiral = dec1_4.Spiral()
//
////    val input = 1024;
//    val input = 325489;
//
//    val steps = input -1
//
//    (1..steps).forEach{ spiral.step() }
//
//    val xDelta = abs(spiral.x)
//    val yDelta = abs(spiral.y)
//    val distance = xDelta + yDelta
//
//    println("distance for $input = $distance")
//
////    spiral.map.forEach {
////        it.forEach {
////            print("$it, ")
////        }
////        println()
////    }
//}
//
////fun printLine(line:Array<Int>) {
////    line.forEach
////}
//
//class dec1_4.Spiral {
//
//    var number = 1
//    var x = 0
//    var y = 0
//
////    var map = Array(7, {Array(7, {0} ) } )
//
//
//    var stepX = dec1_4.create(1, 0, true)
//    var stepY = dec1_4.create(1, 0, false)
//
//    fun step() {
//        number ++
//        val deltaX = stepX.delta()
//        val deltaY = stepY.delta()
//
//        x += deltaX
//        y -= deltaY
//
//        stepX = stepX.next()
//        stepY = stepY.next()
//
////        map[y+3][x+3] = number
//
////        println("n: $number\tx: $x\ty:$y")
//    }
//
//}
//
//class dec1_4.Step( private val radius: Int,
//            private val size:Int,
//            private val delta: Int,
//            private val x: Boolean,
//            private val nextIndex: Int) {
//
//    private var left = size
//
//    fun delta(): Int {
//        left -= 1
//        return delta
//    }
//
//    fun next(): dec1_4.Step {
//        if (left > 0 ) {
//            return this
//        }
//        return dec1_4.create(nextIndex, radius, x)
//    }
//}
//
//
//
//fun dec1_4.create(index: Int, radius: Int, x:Boolean): dec1_4.Step {
//    if (x) { return dec1_4.createX(index, radius) }
//    else { return dec1_4.createY(index, radius) }
//}
//
//fun dec1_4.createX(index: Int, radius: Int): dec1_4.Step {
//    return when (index) {
//        1 -> dec1_4.construct(radius+1, radius+1,1, true, 2)
//        2 -> dec1_4.construct(radius, radius-1,0, true, 3)
//        3 -> dec1_4.construct(radius, radius,0, true, 4)
//        4 -> dec1_4.construct(radius, 2 * radius,-1, true, 5)
//        5 -> dec1_4.construct(radius, 2 * radius,0, true, 6)
//        6 -> dec1_4.construct(radius, radius,1, true, 1)
//        else -> throw IllegalArgumentException("Wrong index: $index")
//    }
//}
//
//fun dec1_4.createY(index: Int, radius: Int): dec1_4.Step {
//    return when (index) {
//        1 -> dec1_4.construct(radius+1, radius+1,0, false, 2)
//        2 -> dec1_4.construct(radius, radius-1,1, false, 3)
//        3 -> dec1_4.construct(radius, radius,1, false, 4)
//        4 -> dec1_4.construct(radius, 2 * radius,0, false, 5)
//        5 -> dec1_4.construct(radius, 2 * radius,-1, false, 6)
//        6 -> dec1_4.construct(radius, radius,0, false, 1)
//        else -> throw IllegalArgumentException("Wrong index: $index")
//    }
//}
//
//
//fun dec1_4.construct(radius: Int, size:Int, delta: Int, x: Boolean, nextIndex: Int):dec1_4.Step {
//    if (size == 0) {
//        return dec1_4.create(nextIndex, radius, x)
//    }
//    else {
//        return dec1_4.Step(radius, size, delta, x, nextIndex)
//    }
//}
//
//
