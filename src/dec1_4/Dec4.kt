package dec1_4

import java.io.File
import java.util.*

/**
 *
 */


fun main(args:Array<String>) {

    var validCount = 0

    File("src/dec4-1.txt").forEachLine { line ->
        val parts = line.split(" ")

        val set = HashSet<String>()

        parts.forEach { set.add(it) }

        if (set.size == parts.size) {
            validCount ++
        }
    }

    println("Valid 1: $validCount")


    validCount = 0

    File("src/dec4-1.txt").forEachLine { line ->
        val parts = line.split(" ")

        val set = HashSet<LinkedList<Char>>()

        parts.forEach {
            val letters = LinkedList<Char>()

            it.forEach { letters.add(it) }
            letters.sort()
            set.add(letters)
        }

        if (set.size == parts.size) {
            validCount ++
        }
    }

    println("Valid2: $validCount")



}