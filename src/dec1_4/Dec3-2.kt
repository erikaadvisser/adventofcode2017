package dec1_4

fun main(args: Array<String>) {

    val spiral = Spiral()


//    val input = 23
    val input = 325489

    val steps = input -1

    (1..steps).forEach{
        spiral.step()
    }

    var map = Array(7, {Array(7, {0} ) } )

    spiral.map.keys.forEach() {
        map[it.x+3][it.y+3] = spiral.map[it] ?: 0
    }




    map.forEach {
        it.forEach {
            print("$it\t")
        }
        println()
    }
}


data class Coordinate(val x:Int, val y:Int)


class Spiral {

    var number = 1
    var x = 0
    var y = 0
    val map = HashMap<Coordinate, Int>()

    init {
        map[Coordinate(0, 0)] = 1
    }

    var stepX = create(1, 0, true)
    var stepY = create(1, 0, false)

    fun step() {
        number ++
        val deltaX = stepX.delta()
        val deltaY = stepY.delta()

        x += deltaX
        y -= deltaY

        val value = adjacentTotal(x,y)

        map[Coordinate(x, y)] = value

        if (value > 325489) {
            println("Value: $value")
            System.exit(0)
        }

        stepX = stepX.next()
        stepY = stepY.next()
    }

    fun adjacentTotal(x:Int, y:Int):Int {
        var total = 0
        total += map[Coordinate(x + 1, y)] ?: 0
        total += map[Coordinate(x - 1, y)] ?: 0
        total += map[Coordinate(x, y + 1)] ?: 0
        total += map[Coordinate(x, y - 1)] ?: 0
        total += map[Coordinate(x + 1, y - 1)] ?: 0
        total += map[Coordinate(x + 1, y + 1)] ?: 0
        total += map[Coordinate(x - 1, y + 1)] ?: 0
        total += map[Coordinate(x - 1, y - 1)] ?: 0

        return total
    }

}

class Step( private val radius: Int,
            private val size:Int,
            private val delta: Int,
            private val x: Boolean,
            private val nextIndex: Int) {

    private var left = size

    fun delta(): Int {
        left -= 1
        return delta
    }

    fun next(): Step {
        if (left > 0 ) {
            return this
        }
        return create(nextIndex, radius, x)
    }
}



fun create(index: Int, radius: Int, x:Boolean): Step {
    if (x) { return createX(index, radius)
    }
    else { return createY(index, radius)
    }
}

fun createX(index: Int, radius: Int): Step {
    return when (index) {
        1 -> construct(radius + 1, radius + 1, 1, true, 2)
        2 -> construct(radius, radius - 1, 0, true, 3)
        3 -> construct(radius, radius, 0, true, 4)
        4 -> construct(radius, 2 * radius, -1, true, 5)
        5 -> construct(radius, 2 * radius, 0, true, 6)
        6 -> construct(radius, radius, 1, true, 1)
        else -> throw IllegalArgumentException("Wrong index: $index")
    }
}

fun createY(index: Int, radius: Int): Step {
    return when (index) {
        1 -> construct(radius + 1, radius + 1, 0, false, 2)
        2 -> construct(radius, radius - 1, 1, false, 3)
        3 -> construct(radius, radius, 1, false, 4)
        4 -> construct(radius, 2 * radius, 0, false, 5)
        5 -> construct(radius, 2 * radius, -1, false, 6)
        6 -> construct(radius, radius, 0, false, 1)
        else -> throw IllegalArgumentException("Wrong index: $index")
    }
}


fun construct(radius: Int, size:Int, delta: Int, x: Boolean, nextIndex: Int): Step {
    if (size == 0) {
        return create(nextIndex, radius, x)
    }
    else {
        return Step(radius, size, delta, x, nextIndex)
    }
}


