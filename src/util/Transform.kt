package util

/**
 *
 */
class Transform {

    fun toBinary(hash: String):String {
        val result = StringBuilder()

        (0..15).forEach {
            val part = hash.substring(it*2, 2 + it*2)
            val resultPart = hexTupleToBinary(part)
            result.append(resultPart)
        }

        return result.toString()
    }

    fun hexTupleToBinary(tuple: String): String {
        val asInteger = Integer.parseUnsignedInt(tuple, 16)

        val result = StringBuilder()
        (0..7).forEach {
            val mask = 1.shl(it)
            val test = asInteger.and(mask)
            if (test != 0 ) {
                result.append('1')
            }
            else {
                result.append('0')
            }
        }
        result.reverse()

        return result.toString()
    }


    fun toHex(input: Int):String {
        val part = input.toString(16)
        if (input < 16) {
            return "0" + part
        }
        return part
    }

    fun intToBinary(input: Int): String {
        val result = StringBuilder()
        (0..31).forEach {
            val mask = 1.shl(it)
            val test = input.and(mask)
            if (test != 0 ) {
                result.append('1')
            }
            else {
                result.append('0')
            }
        }
        result.reverse()

        return result.toString()
    }

}