package util

import java.io.File
import java.io.File.separator

/**
 *
 */
class FileInput(val location: String) {


    fun allTextAsList(separator: String): List<String> {
        val fileText = File(location).readLines(charset("US-ASCII"))[0]

        return fileText.split(separator)
    }

    fun toList(): List<String> {
        return File(location).readLines(charset("US-ASCII"))
    }

    fun toMap(separator: String): Map<String, String> {
        val lines = File(location).readLines(charset("US-ASCII"))
        val map = HashMap<String, String>()
        lines.forEach {
            val parts = it.split(separator)
            map[parts[0]] = parts[1]
        }
        return map
    }

}