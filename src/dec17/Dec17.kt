package dec17

import kotlin.system.measureTimeMillis

/**
 *
 */
fun main(args: Array<String>) {

    val simpleLock = SpinLockSimple(2017, 348)
    val after2017 = simpleLock.after(2017)
    println("part1: $after2017")

    val millis = measureTimeMillis {
        val optimizedLock = SpinLockOptimized(50_000_000, 348)
        val after0 = optimizedLock.after(0)
        println("part2: $after0")
    }

    println("Total time: ${millis / 1000 * 60} minutes = ${millis / 1000} seconds = ${millis} millis ")



//    var time = 0L
//
//    time = measureTimeMillis {
//        SpinLock(1000, 348)
//    }
//    println("time: $time - 1000")
//
//    time = measureTimeMillis {
//        SpinLock(10_000, 348)
//    }
//    println("time: $time - 10_000")
//
//    time = measureTimeMillis {
//        SpinLock(20_000, 348)
//    }
//    println("time: $time - 20_000")
//
//    time = measureTimeMillis {
//        SpinLock(100_000, 348)
//    }
//    println("time: $time - 100_000")
//
//    time = measureTimeMillis {
//        SpinLock(200_000, 348)
//    }
//    println("time: $time - 200_000")
//
//    time = measureTimeMillis {
//        SpinLock(5200_000, 348)
//    }
//    println("time: $time - 200_000")

}