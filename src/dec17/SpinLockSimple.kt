package dec17

import java.util.*

/**
 *
 */
class SpinLockSimple(maxValue: Int, val stepSize: Int) {

//    val buffer = LinkedList<Int>()
    val buffer = ArrayList<Int>(maxValue)
    var currentPosition = 0

    init {
        buffer.add(0)
        (1..maxValue).forEach { value ->
            currentPosition = (currentPosition + stepSize) % buffer.size + 1
            buffer.add(currentPosition, value)
//            println("$buffer")
            if ( value % 100_000 == 0) {
                println("Initializing at: $value")
            }
        }
    }


    fun after(value: Int): Int {
        val index = buffer.indexOf(value)
        val nextIndex = (index + 1) % buffer.size
        return buffer[nextIndex]
    }


}