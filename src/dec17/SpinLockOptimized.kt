package dec17

/**
 *
 */
class SpinLockOptimized(maxValue: Int, val stepSize: Int) {

    val buffer = CursorLinkedList(0)

    init {
        val initial = System.currentTimeMillis()
        var snap = initial

        (1..maxValue).forEach { value ->
            buffer.move(stepSize)
            buffer.insertAfterCursor(value)
            if ( value % 100_000 == 0) {

                val snap2 = System.currentTimeMillis()
                val spent = snap2 - snap
                snap = snap2
                val total = snap2 - initial

                println("Initializing at: $value - took $spent -- (total: $total}")
            }
        }
    }

    fun after(value: Int): Int {
        return buffer.valueAfter(value)
    }
}