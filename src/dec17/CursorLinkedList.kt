package dec17

/**
 *
 */
class CursorLinkedList(firstValue: Int) {

    private val first = Element(firstValue, null)
    private var cursor = first

    init {
        first.next = first
    }

    fun move(steps: Int) {
        (1..steps).forEach {
            cursor = cursor.next!!
        }
    }

    fun insertAfterCursor(value: Int) {
        val inserted = Element(value, cursor.next)
        cursor.next = inserted
        cursor = inserted
    }

    fun valueAfter(toFind: Int): Int {
        var current = first
        while (true) {
            if (current.value == toFind) {
                return current.next?.value ?: -1
            }
        }
    }
}

private data class Element(val value: Int, var next: Element?)