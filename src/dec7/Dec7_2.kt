package dec7

import java.io.File
import java.lang.IllegalStateException
import java.nio.charset.Charset


fun main(args:Array<String>) {
    val result = Dec7_2().run("dec7-2.txt")

    println("root: $result")
}

class Dec7_2 {

    fun run(fileName:String):Int {
        val lines = loadFile(fileName)
        val rootName = findRoot(lines)

        val root:Node = createTree(rootName, lines, 0)

        root.print()

//        val totalWeight = tree.totalWeight()

        return -1
    }

    fun createTree(rootName: String, lines: List<String>, level:Int):Node {
        val line = lines.find { it.startsWith(rootName) }!!

        val childNames = childNames(line)
        val childNodes:List<Node> = childNames.map {
            createTree(it, lines, level + 1)
        }
        val weight = parseWeight(line)

        return Node(rootName, weight, childNodes, level)
    }

    private fun parseWeight(line: String): Int {
        val leftPart = line.split("(")[1]
        val number = leftPart.split(")")[0]
        val weight = Integer.parseInt(number)
        return weight
    }


    fun findRoot(lines:List<String>):String {
        val histogram = createHistogram(lines)

        histogram.entries.forEach {
            if (it.value == 1) {
                return it.key
            }
        }
        TODO("should not happen")
    }

    fun createHistogram(lines: List<String>): Map<String, Int> {
        val map = HashMap<String, Int>()

        lines.forEach {
            val leaves = parseLine(it)
            leaves.forEach {
                if (map[it] != null) {
                    map[it] = map[it]!!+1
                }
                else {
                    map[it] = 1
                }
            }
        }

        return map

    }

    private fun parseLine(line: String): List<String> {
        val leaves = ArrayList<String>()
        val parts:List<String> = line.split(" ")

        leaves.add(parts[0])
        leaves.addAll(childNames(line))

        return leaves
    }

    private fun childNames(line:String): List<String> {
        val children = ArrayList<String>()
        val parts:List<String> = line.split(" ")

        if (parts.size > 3) {
            children.addAll(parts.subList(3, parts.size ))
        }
        return children
    }

    private fun loadFile(fileName: String): List<String> {
        val fullName = "src/dec7/$fileName"
        val normalizedLines = ArrayList<String>()

        File(fullName).readLines(Charset.forName("US-ASCII")).forEach {
            val normalized = it.replace(", ", " ")
            normalizedLines.add(normalized)
        }

        return normalizedLines
    }
}

class Node (
        val name:String,
        val weight:Int,
        val children: List<Node>,
        val level:Int) {

    fun totalWeight(): Int {

        val childrenWeight = children.map { it.totalWeight() }

        val min = childrenWeight.min()
        val max = childrenWeight.max()


//        if (min != max) {
//            println("weight mismatch: $name ($level)")
//            children.forEach {
//                val childWeight = it.totalWeight()
//                println(".. ${it.name} = ${childWeight}")
//            }
//            println()
//        }
        return childrenWeight.sum() + weight
    }

    fun print() {
        val indent = StringBuffer();
        (0..level * 4).forEach { indent.append(' ') }

        var balanceString:String
        val balanced = checkBalanced(children)
        if (balanced) {
            balanceString = "balanced"
        } else {
            if (children.size < 3) {
                balanceString = "unbalanced, size <3"
            }
            else {
                val unbalancedNode = findUnbalancedNode(children)
                val balancedNode = findBalancedNode(children, unbalancedNode)

                val weightDifference = balancedNode.totalWeight() - unbalancedNode.totalWeight()
                balanceString = "unbalanced, ${unbalancedNode.name} must adjust by ${weightDifference} to ${unbalancedNode.weight + weightDifference}"
            }
        }

        println("${indent}$name ${weight} / ${totalWeight()} ${balanceString}")
        children.forEach { it.print() }
    }


    private fun checkBalanced(children: List<Node>): Boolean {
        if (children.isEmpty()) { return true }
        val childrenWeight = children.fold(0) { total, next -> total + next.totalWeight() }
        val firstChildWeight = children[0].totalWeight()

        return (childrenWeight == children.size * firstChildWeight)
    }

    private fun findUnbalancedNode(children: List<Node>): Node {
        children.forEach { candidate ->
            val subsetWithoutCandidate = children.toMutableList()
            subsetWithoutCandidate.remove(candidate)

            if (checkBalanced(subsetWithoutCandidate)) {
                return candidate
            }
        }
        throw IllegalStateException("did not find unbalanced node")
    }

    private fun findBalancedNode(children: List<Node>, unbalanced: Node): Node {
        return children.find { it != unbalanced }!!
    }



}