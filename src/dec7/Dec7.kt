package dec7

import java.io.File
import java.nio.charset.Charset


fun main(args:Array<String>) {
    val result = Dec7().run("dec7-2.txt")

    println("root: $result")
}

class Dec7 {

    fun run(fileName:String):String {
        val lines = loadFile(fileName)
        val nodes = createNodes(lines)


        nodes.entries.forEach {
            if (it.value == 1) {
                return it.key
            }
        }
        TODO("should not happen")
    }

    fun createNodes(lines: List<String>): Map<String, Int> {
        val map = HashMap<String, Int>()

        lines.forEach {
            val leaves = parseLine(it)
            leaves.forEach {
                if (map[it] != null) {
                    map[it] = map[it]!!+1
                }
                else {
                    map[it] = 1
                }
            }
        }

        return map

    }

    private fun parseLine(line: String): List<String> {
        val normalizedLine = line.replace(',', ' ')
        val leaves = ArrayList<String>()
        val parts:List<String> = normalizedLine.split(" ")

        leaves.add(parts[0])
        if (parts.size > 3) {
            leaves.addAll(parts.subList(3, parts.size ))
        }

        return leaves
    }

    private fun loadFile(fileName: String): List<String> {
        val fullName = "src/dec7/$fileName"
        return File(fullName).readLines(Charset.forName("US-ASCII"))
    }
}