package dec10

import java.util.*
import java.util.Arrays.asList
import kotlin.coroutines.experimental.EmptyCoroutineContext.fold

/**
 *
 */


fun testXor(args: Array<String>) {
//    fun main(args: Array<String>) {

    val a = "65 ^ 27 ^ 9 ^ 1 ^ 4 ^ 3 ^ 40 ^ 50 ^ 91 ^ 7 ^ 6 ^ 0 ^ 2 ^ 5 ^ 68 ^ 22"
    val b = a.split(" ^ ").map { Integer.parseInt(it) }
    val total = b.fold(0) { total, it -> total.xor(it) }

    println("total $total")

    val f = 1
    val g = 17
    println("${Integer.toHexString(f)}, ${g.toString(16)}")
}

fun main(args: Array<String>) {
//fun test(){

//    val input = ""
    val input = "225,171,131,2,35,5,0,13,1,246,54,97,255,98,254,110"

    val hasher = KnotHash(input)
    val result = hasher.run()

    println("result: $result")
}



