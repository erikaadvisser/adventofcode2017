package dec10

import java.util.*

/**
 *
 */
class KnotHash(input: String) {
    private val lengths = ArrayList<Int>()
    private val ring = Ring()

    init {
        val asciiCodes = input.map{ it.toInt() }
        lengths.addAll( asciiCodes )
        lengths.addAll(Arrays.asList(17, 31, 73, 47, 23))
    }

    fun run():String {
        (1..64).forEach {
            round()
        }
        return ring.denseHash()
    }

    fun round() {
        lengths.forEach { length ->
            ring.fold(length)
        }
    }
}