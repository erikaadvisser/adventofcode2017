package dec10

import java.util.*
import kotlin.coroutines.experimental.EmptyCoroutineContext.fold

/**
 *
 */


fun main(args: Array<String>) {




    val input = "225,171,131,2,35,5,0,13,1,246,54,97,255,98,254,110"
//    val inputNumbers = input.split(",").fold(LinkedList<Int>()) { list, it ->
//        list.add(Integer.parseInt(it))
//        list
//    }
//
//    val inputNumbers = LinkedList<Int>()
//    input.split(",").forEach {
//        inputNumbers.add(Integer.parseInt(it))
//    }

    val inputNumbers: List<Int> = input.split(",").map { Integer.parseInt(it) }

    val result = Dec10(inputNumbers).run()

    println("result: $result")


}

private class Dec10(private val lengths: List<Int>) {
    private val ring = Ring()

    fun run(): Int {
        lengths.forEach {
            fold(it)
        }
        return ring.customHash()
    }

    fun fold(length: Int) {
        ring.fold(length)
    }
}

