package dec10

import java.util.ArrayList

class Ring {

    private val elements = ArrayList<Int>()
    private var cursor = 0
    private var skip = 0

    init {
        (0..255).forEach { i ->
            elements.add(i)
        }
    }

    fun customHash(): Int {
        return elements[0] * elements[1]
    }

    fun fold(length: Int) {
        val subList = subList(cursor, length)
        val reversed = subList.asReversed()
        replace(cursor, reversed)
        cursor = loop(cursor + length + skip)
        skip ++
    }

    private fun subList(cursor: Int, length: Int): List<Int> {
        val subList = ArrayList<Int>()

        (0 until length).forEach { i ->
            val index = loop(cursor + i)
            val value = elements[index]
            subList.add(value)
        }
        return subList
    }

    private fun replace(cursor: Int, subList: List<Int>) {
        (0 until subList.size).forEach { i ->
            val index = loop(cursor + i)
            val value = subList[i]
            elements[index] = value
        }
    }

    private fun loop(input: Int):Int {
        return input % elements.size
    }

    fun denseHash(): String {
        val blocks = toBlocks()
        val xors = blocks.map { xor(it) }
        val hex = xors.fold("") { total, it -> total + toHex(it) }

        return hex
    }

    private fun toBlocks(): List<List<Int>> {
//        val blocks = ArrayList<List<Int>>()
//        (0..15).forEach { i ->
//            val block = elements.subList((i*16), (i+1) * 16)
//            blocks.add(block)
//        }

        val blocks = (0..15).map { i ->
            elements.subList((i*16), (i+1) * 16)
        }

        return blocks
    }

    private fun xor(block: List<Int>):Int {
        val total = block.fold(0) { total, it -> total.xor(it) }

        return total
    }

    private fun toHex(input: Int):String {
        val part = input.toString(16)
        if (input < 16) {
            return "0" + part
        }
        return part

    }

}