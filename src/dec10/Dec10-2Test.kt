package dec10

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

/**
 *
 */
class Dec10_2Test {

    @Test
    fun `empty string`() {
        val hash = KnotHash("").run()
        assertEquals("a2582a3a0e66e6e86e3812dcb672a272", hash)
    }


}