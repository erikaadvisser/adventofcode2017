package dec21

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 *
 */
internal class PatternTest {

    @Test
    fun `test split2 stich`() {
        val start = "#..#/..../..../#..#"
        println(start)
        val pattern = Pattern(start, HashMap())
        val lines = pattern.split(start)
        println(lines)
        assertEquals( "[#./.., .#/.., ../#., ../.#", lines.toString())

        val stiched = pattern.stich(lines)
        println(stiched)
        assertEquals(start, stiched)
    }

    @Test
    fun `test split3 stich`() {
        val start = ".....#.##/.....#.##/.....#.##/.....#.##/.....#.##/.....#.##/.....#.##/.....#.##/.....#.##"
        println(start)

        val pattern = Pattern(start, HashMap())

        val lines = pattern.split(start)
        println(lines)
        assertEquals( "[.../.../..., ..#/..#/..#, .##/.##/.##, .../.../..., ..#/..#/..#, .##/.##/.##, .../.../..., ..#/..#/..#, .##/.##/.##]", lines.toString())

        val stiched = pattern.stich(lines)
        println(stiched)
        assertEquals(start, stiched)
    }

    @Test
    fun `test flip`() {
        val pattern = Pattern("", HashMap())

        val res = pattern.flip(".#./..#/###")
        println(res)
        assertEquals(".#./#../###", res)
    }

    @Test
    fun `test rotate 2x2`() {
        val pattern = Pattern("", HashMap())

        val f1 = pattern.rotate("#./..")
        println(f1)
        assertEquals(".#/..", f1)

        val f2 = pattern.rotate(f1)
        println(f2)
        assertEquals("../.#", f2)

        val f3 = pattern.rotate(f2)
        println(f3)
        assertEquals("../#.", f3)

        val f4 = pattern.rotate(f3)
        println(f4)
        assertEquals("#./..", f4)

    }

    @Test
    fun `test rotate 3x3`() {
        val pattern = Pattern("", HashMap())

        var r = "##./.../..."

        r = pattern.rotate(r)
        println(r)
        assertEquals("..#/..#/...", r)

        r = pattern.rotate(r)
        println(r)
        assertEquals(".../.../.##", r)

        r = pattern.rotate(r)
        println(r)
        assertEquals(".../#../#..", r)

        r = pattern.rotate(r)
        println(r)
        assertEquals("##./.../...", r)

        r = pattern.rotate("..#/.#./...")
        println(r)
        assertEquals(".../.#./..#", r)

    }


}