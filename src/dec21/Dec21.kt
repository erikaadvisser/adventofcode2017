package dec21

import util.FileInput
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    println("go")
    val time = measureTimeMillis {
        val map = FileInput("src/dec21/21-input.txt").toMap(" => ")
//    val map = FileInput("src/dec21/example.txt").toMap(" => ")
        val start = ".#./..#/###"
        val pattern = Pattern(start, map)
        val result = pattern.run()
        println("part1: pixels on after 5 = $result")
    }
    println("millies: $time")
}


class Pattern(val start: String,
              ruleInput: Map<String, String>) {


    private val rule = HashMap<String, String>()

    init {
        ruleInput.keys.forEach { ruleKey ->
            val ruleOutput = ruleInput[ruleKey] !!
            rule.put(ruleKey, ruleOutput)
            rule.put(rotate(ruleKey), ruleOutput)
            rule.put(rotate(rotate(ruleKey)), ruleOutput)
            rule.put(rotate(rotate(rotate(ruleKey))), ruleOutput)

            rule.put(flip(ruleKey), ruleOutput)
            rule.put(rotate(flip(ruleKey)), ruleOutput)
            rule.put(rotate(rotate(flip(ruleKey))), ruleOutput)
            rule.put(rotate(rotate(rotate(flip(ruleKey)))), ruleOutput)
        }
    }

    fun run(): Int {
        var current = start
        (1..18).forEach {
            current = enhance(current)
        }

        return current.count { it == '#' }
    }

    fun enhance(input: String): String {
        val parts = split(input)
        val enhancedParts = parts.map {
//            if (!rule.containsKey(it)){
//                println("no rule found for: $it")
//            }
            rule[it]!!
        }
        val enhanced = stich(enhancedParts)
        return enhanced
    }

    fun stich(blocks: List<String>): String {
        val blockSize = blocks[0].split("/")[0].length
        val columns = Math.sqrt(blocks.size.toDouble()).toInt()

        val result = StringBuilder()
        for (block in 0 until columns) {

            for (subBlock in 0 until blockSize) {
                val lineBuilder = StringBuilder()
                for (column in 0 until columns) {

                    val blockParts = blocks[block * columns + column].split("/")
                    val part = blockParts[subBlock]

                    lineBuilder.append (part)
                }
                result.append(lineBuilder.toString())
                result.append("/")
            }
        }
        return result.removeSuffix("/").toString()
    }

    fun split(input: String): List<String> {
        val lines = input.split("/")

        if (lines.size % 2 == 0) {
            return split2(lines)
        }
        return split3(lines)
    }

    fun split2(lines: List<String>): List<String> {
        val result = ArrayList<String>()
        for (i in 0 until lines.size step 2) {
            val topLine = lines[i]
            val bottomLine = lines[i+1]

            for (j in 0 until lines.size step 2) {
                val top = topLine.substring(j, j + 2)
                val bottom = bottomLine.substring(j, j + 2)

                result.add(top + "/" + bottom)
            }
        }
        return result
    }

    fun split3(lines: List<String>): List<String> {
        val result = ArrayList<String>()
        for (i in 0 until lines.size step 3) {
            val topLine = lines[i]
            val middleLine = lines[i + 1]
            val bottomLine = lines[i + 2]

            for (j in 0 until lines.size step 3) {
                val top = topLine.substring(j, j + 3)
                val middle = middleLine.substring(j, j + 3)
                val bottom = bottomLine.substring(j, j + 3)

                result.add(top + "/" + middle + "/" + bottom)
            }
        }
        return result
    }


    fun rotate(ruleKey: String): String {
        if (ruleKey.length == 5) {
            val nw = ruleKey[0]
            val ne = ruleKey[1]
            val sw = ruleKey[3]
            val se = ruleKey[4]

            return "" + sw + nw + "/" + se + ne
        }
        val nw =ruleKey[0]
        val nn =ruleKey[1]
        val ne =ruleKey[2]
        val ww =ruleKey[4]
        val mm =ruleKey[5]
        val ee =ruleKey[6]
        val sw =ruleKey[8]
        val ss =ruleKey[9]
        val se =ruleKey[10]

        return "" + sw + ww + nw + "/" + ss + mm + nn + "/" + se + ee + ne
    }

    fun flip(ruleKey: String): String {
        val parts = ruleKey.split("/")
        val flippedParts = parts.map { it.reversed() }
        val result = flippedParts.fold("") { total, it -> total + it + "/" }
        return result.removeSuffix("/")
    }


}

