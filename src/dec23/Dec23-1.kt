package dec23

import com.sun.javafx.animation.TickCalculation.sub
import util.FileInput
import java.math.BigInteger
import java.util.*
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val time = measureTimeMillis { run() }
    println("time: $time millis")
}

fun run() {
    val lines = FileInput("src/dec23/23-input.txt").toList()

    val coProcessor = CoProcessor(lines)
    coProcessor.run()

    println("mulCount = ${coProcessor.mulCount}")
}

class CoProcessor(val instructions:List<String>) {

    val registers = HashMap<String, BigInteger>()
    var sound = BigInteger.ZERO
    var instructionIndex = 0

    var mulCount = 0

    fun run() {
       while(instructionIndex >= 0 && instructionIndex < instructions.size ) {
           if (instructionIndex < 0 || instructionIndex >= instructions.size) {
               println("abort - current $instructionIndex")
               exit()
           }
           val line = instructions[instructionIndex]
           val scanner = Scanner(line)
           val command = scanner.next()
           val arg1 = scanner.next()
           val arg2 = if (scanner.hasNext()) scanner.next() else ""

           when(command) {
               "set" -> set(arg1, arg2)
               "add" -> add(arg1, arg2)
               "sub" -> sub(arg1, arg2)
               "mul" -> mul(arg1, arg2)
               "mod" -> mod(arg1, arg2)
               "jgz" -> jgz(arg1, arg2)
               "jnz" -> jnz(arg1, arg2)
               "snd" -> snd(arg1)
               "rcv" -> rcv(arg1)
               else -> TODO("unimplemented: $command")
           }

           instructionIndex ++
       }
    }

    private fun add(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current + parse(arg2)
        set(arg1, new)
    }

    private fun sub(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current - parse(arg2)
        set(arg1, new)
    }

    private fun mul(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current * parse(arg2)
        set(arg1, new)
        mulCount ++
    }

    private fun mod(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current % parse(arg2)
        set(arg1, new)
    }

    private fun jnz(arg1: String, arg2: String) {
        val current = parse(arg1)
        if (current != BigInteger.ZERO) {
            instructionIndex --
            instructionIndex += parse(arg2).toInt()
        }
    }

    private fun jgz(arg1: String, arg2: String) {
        val current = parse(arg1)
        if (current > BigInteger.ZERO) {
            instructionIndex --
            instructionIndex += parse(arg2).toInt()
        }
    }


    private fun snd(arg1: String) {
        sound = parse(arg1)
    }

    private fun rcv(arg1: String) {
        error("not implemented")
//        if (parse(arg1) !=  BigInteger.ZERO) {
//            println("recoverd: $sound")
//            exit()
//        }
    }


    fun set(arg1: String, arg2: String) {
        val value = parse(arg2)
        set(arg1, value)
    }

    fun parse(arg: String):BigInteger {
        try {
            val value = BigInteger(arg)
            return value
        }
        catch (e: NumberFormatException) {
            return registers.getOrPut(arg, {BigInteger.ZERO})
        }
    }

    fun set(arg1: String, value: BigInteger) {
        registers.put(arg1, value)
    }


    private fun exit() {
        System.exit(0)
    }
}

