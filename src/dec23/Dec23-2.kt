package dec23

import util.FileInput
import java.util.*

fun main(args: Array<String>) {

    val lines = FileInput("src/dec23/23-input-optimized.txt").toList()
    val instructions = lines.map { it -> Instruction(it)}

    val program = Program(instructions)

    val start = System.currentTimeMillis()
    var mid = start

    var count = 0L
    do {
        count ++
        if (count % 10_000_000L == 0L) {
            print("$count: ")
            val now = System.currentTimeMillis()
            println(" busy ($count): ${(now - mid) / 1000} total: ${(now - start) / 1000} average = ${1000 * count / (now-start)}/s \t\t")
            mid = now
        }

        val terminated = program.runOnce()
    } while (!terminated)

    println("h = ${program.registers["h"]}")
}


fun s15(value: Long?): String {
    return String.format("%-15d", value?: 0L)
}

fun s13(value: Long?): String {
    return String.format("%-13d", value?: 0L)
}

fun s6(value: Long?): String {
    return String.format("%-6d", value?: 0L)
}

fun i5(value: Int): String {
    return String.format("%-5d", value)
}

fun t(value: String): String {
    return String.format("%-6s", value)
}


class Program(private val instructions: List<Instruction>) {

    val registers = HashMap<String, Long>()
    private var instructionIndex = 0
    var count = 0

    init {
        registers["a"] = 1L
        registers["b"] = 0L
        registers["c"] = 0L
        registers["d"] = 0L
        registers["e"] = 0L
        registers["f"] = 0L
        registers["g"] = 0L
        registers["h"] = 0L
    }

    fun runOnce():Boolean {

        if (instructionIndex < 0 || instructionIndex >= instructions.size) {
            println("abort - current $instructionIndex")
            return true
        }
        val instruction = instructions[instructionIndex]

        when (instruction.command) {
            "set" -> set(instruction)
            "add" -> add(instruction)
            "sub" -> sub(instruction)
            "mul" -> mul(instruction)
            "jnz" -> jnz(instruction)
            "nop" -> nop()
            else -> error("unimplemented: $instruction.command")
        }

        instructionIndex++


        return false
    }

    private fun nop() {
        // la la la la
    }

    private fun stateToString(instruction: Instruction): String {
        val state = StringBuilder()
        state.append("p: ${s15(registers["p"])}")
        state.append("a: ${s13(registers["a"])}")
        state.append("b: ${s13(registers["b"])}")
        state.append("f: ${s6(registers["f"])}")
        state.append("i: ${s6(registers["i"])}")
        return state.toString()
    }

    private fun add(instruction: Instruction) {
        val current = registers.get(instruction.arg1)!!
        val new = current + instruction.parseArg2(registers)
        set(instruction.arg1, new)
    }

    private fun mul(instruction: Instruction) {
        val current = registers.get(instruction.arg1)!!
        val new = current * instruction.parseArg2(registers)
        set(instruction.arg1, new)
    }

    private fun sub(instruction: Instruction) {
        val current = registers.get(instruction.arg1)!!
        val new = current - instruction.parseArg2(registers)
        set(instruction.arg1, new)
    }

    private fun jnz(instruction: Instruction) {
        val evaluate = instruction.parseArg1(registers)
        if (evaluate != 0L) {
            instructionIndex--
            instructionIndex += instruction.parseArg2(registers).toInt()
        }
    }

    private fun set(instruction: Instruction) {
        val value = instruction.parseArg2(registers)
        set(instruction.arg1, value)
    }

    private fun set(arg1: String, value: Long) {
        registers.put(arg1, value)
    }


}



data class Instruction(val input: String) {

    val command = parseCommand(input)
    val arg1 = parseArg1(input)
    val arg2 = parseArg2(input)

    val arg1BigInt = parseBigInt(arg1)
    val arg2BigInt = parseBigInt(arg2)


    private fun parseBigInt(arg: String): Long? {
//        if ()
        try {
            val value = arg.toLong()
            return value
        } catch (e: NumberFormatException) {
            return null
        }
    }

    fun parseArg1(registers: HashMap<String, Long>): Long {
        if (arg1BigInt != null) {
            return arg1BigInt
        }
        else {
            return registers.getOrPut(arg1, { 0L })
        }
    }

    fun parseArg2(registers: HashMap<String, Long>): Long {
        if (arg2BigInt != null) {
            return arg2BigInt
        }
        else {
            return registers.getOrPut(arg2, { 0L })
        }
    }



    private fun parseCommand(input: String): String {
        val scanner = Scanner(input)
        return scanner.next()
    }


    private fun parseArg1(input: String): String {
        val scanner = Scanner(input)
        scanner.next()
        return scanner.next()
    }


    private fun parseArg2(input: String): String {
        val scanner = Scanner(input)
        scanner.next()
        scanner.next()
        if (scanner.hasNext()) {
            return scanner.next()
        }
        return ""
    }

    override fun toString(): String {
        return "$command $arg1 ${t(arg2)}"
    }
}
