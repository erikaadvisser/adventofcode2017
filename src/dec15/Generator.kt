package dec15

import util.Transform

/**
 *
 */
class Generator(var a: Int, var b: Int) {

    val mask: Int = 0x0000ffff
    val transform = Transform()

    init {
        printBits("mask ", mask)
    }


    private fun matchLower16(ca: Int, cb: Int): Boolean {

//        printBits("a ", ca)
//        printBits("b ", cb)

//        println("a: $ca")
//        println("b: $cb")

        val aLower = ca.and(mask)
        val bLower = cb.and(mask)

//        printBits("a lower", aLower)
//        printBits("b lower", bLower)
//        println()

        return aLower == bLower
    }

    private fun printBits(text: String, input: Int) {
        println("$text ${transform.intToBinary(input)}")
    }

    fun run1(rounds: Int): Int {
        var total = 0
        (0 until rounds).forEach {
            a = calcA()
            b = calcB()

            if (matchLower16(a, b)) {
                total++
            }
        }
        return total
    }


    fun run2(totalPairs: Int): Int {
        val aResults = ArrayList<Int>()
        val bResults = ArrayList<Int>()

        var pairs = 0
        while (pairs < totalPairs) {
            a = calcA()
            b = calcB()

            if (a % 4 == 0) {
                aResults.add(a)
            }
            if (b % 8 == 0) {
                bResults.add(b)
            }

            pairs = Math.min(aResults.size, bResults.size)
        }


        var total = 0

        (0 until totalPairs).forEach { i ->
            val aValue = aResults[i]
            val bValue = bResults[i]
//            println("a: $aValue")
//            println("b: $bValue")
            if (matchLower16(aValue, bValue)) {
//                println("pair: $i produces hit ")
//                println("a: $aValue")
//                println("b: $bValue")
                total++
            }
        }

        return total
    }


    private fun calcA(): Int {
        return calc(a, 16807L)
    }

    private fun calcB(): Int {
        return calc(b, 48271L)
    }

    private fun calc(input: Int, factor: Long): Int {
        val mul: Long = (input.toLong() * factor)
        val res = (mul % 2147483647).toInt()
        return res
    }

}