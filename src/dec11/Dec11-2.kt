package dec11

import java.io.File
import java.lang.Integer.max
import java.lang.Integer.min
import java.nio.charset.Charset
import java.util.*
import kotlin.math.abs
import kotlin.math.floor


fun main(args: Array<String>) {

    val input = File("src/dec11/dec-11-1.txt").readText(Charset.forName("US-ASCII")).split(",")

    val result = Dec11_2(input).run()

    println("Result: $result")
}

/**
 *
 */
class Dec11_2(private val directions:List<String>) {

    private val half:Double = Math.sin(Math.PI /3)

    fun run():Int {

        var x = 0.0
        var y = 0.0

        val distances = LinkedList<Int>()

        directions.forEach {
            when(it) {
                "n" ->  { y -= 1 }
                "ne" -> { y -= half; x += 1 }
                "nw" -> { y -= half; x -= 1 }
                "s" ->  { y += 1 }
                "se" -> { y += half; x += 1 }
                "sw" -> { y += half; x -= 1 }
            }
            distances.add(distance(x, y))
        }

        println("x: $x, y: $y")// x: 1326, y: -181

//        return distance(x,y)
        return distances.max() ?: 0
    }

    fun distance(xi: Double, yi: Double): Int {
        val x = abs(xi)
        val y = abs(yi)

        val max = java.lang.Double.max(x, y)

        var steps = max

        if (floor(max) != max) {
            steps += 1
        }

        return floor(steps).toInt()
    }
}