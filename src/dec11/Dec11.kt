package dec11

import java.io.File
import java.lang.Double.max
import java.lang.Double.min
import java.nio.charset.Charset
import kotlin.math.abs
import kotlin.math.floor


fun main(args: Array<String>) {

    val input = File("src/dec11/dec-11-1.txt").readText(Charset.forName("US-ASCII")).split(",")
//
    val result = Dec11(input).run()


//
//    val result =1
    println("Result: $result")

}

/**
 *
 */
class Dec11(private val directions:List<String>) {

    private val half:Double = Math.sin(Math.PI /3)

    fun run():Int {

        var x = 0.0
        var y = 0.0

        directions.forEach {
            when(it) {
                "n" ->  { y -= 1 }
                "ne" -> { y -= half; x += 1 }
                "nw" -> { y -= half; x -= 1 }
                "s" ->  { y += 1 }
                "se" -> { y += half; x += 1 }
                "sw" -> { y += half; x -= 1 }
            }
        }

        println("x: $x, y: $y")// x: 1326, y: -181

        x = abs(x)
        y = abs(y)

        val max = max(x, y)
        val min = min(x, y)

        var steps = max

//        if (min % 2 == 1) {
//            steps += 1
//        }

        if (floor(max) != max) {
            steps += 1
        }

        return floor(steps).toInt()
    }

}