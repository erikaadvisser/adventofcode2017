package dec11

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

/**
 *
 */
class Dec11_1Test {

    @Test
    fun `1`() {
        val underTest = Dec11("ne,ne,ne".split(","))
        assertEquals(3, underTest.run())
    }

    @Test
    fun `2`() {
        val underTest = Dec11("ne,ne,sw,sw".split(","))
        assertEquals(0, underTest.run())
    }

    @Test
    fun `3`() {
        val underTest = Dec11("ne,ne,s,s".split(","))
        assertEquals(2, underTest.run())
    }

    @Test
    fun `4`() {
        val underTest = Dec11("se,sw,se,sw,sw".split(","))
        assertEquals(3, underTest.run())
    }

    @Test
    fun `5`() {
        val underTest = Dec11("ne,ne,ne,n".split(","))
        assertEquals(4, underTest.run())
    }

}