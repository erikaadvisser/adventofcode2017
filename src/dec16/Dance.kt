package dec16

import java.nio.file.Files.move

class Dance(val dancersStart: String) {

    var dancers = dancersStart
    val size = dancers.length

    fun run(moves: List<String>): String {
        moves.forEach { doMove(it) }

        return dancers
    }

    private inline fun doMove(move: String) {
        val type = move[0]
        val input = move.substring(1)

        when(type) {
            's' -> spin(input)
            'x' -> exchange(input)
            'p' -> partner(input)
            else -> error("unexpected move type $type")
        }

    }

    private inline fun spin(input: String) {

        val number = Integer.parseInt(input)
        val endPart = dancers.substring(size - number)
        val frontPart = dancers.substring(0, size - number)

        dancers = endPart + frontPart
    }

    private inline fun exchange(input: String) {
        val parts = input.split("/")
        val a = Integer.parseInt(parts[0])
        val b = Integer.parseInt(parts[1])

        val dancerArray = dancers.toCharArray()

        val dancerA = dancerArray.get(a)
        val dancerB = dancerArray.get(b)

        dancerArray.set(a, dancerB)
        dancerArray.set(b, dancerA)

        dancers = dancerArray.joinToString("")
    }

    private inline fun partner(input: String) {
        val parts = input.split("/")
        val dancerA = parts[0].toLowerCase().get(0)
        val dancerB = parts[1].toLowerCase().get(0)

        val dancerArray = dancers.toCharArray()

        val a = dancerArray.indexOf(dancerA)
        val b = dancerArray.indexOf(dancerB)

        dancerArray.set(a, dancerB)
        dancerArray.set(b, dancerA)

        dancers = dancerArray.joinToString("")
    }

    fun runMutations(count: Int): String {
        val mutations = ArrayList<Int>()

        dancersStart.forEachIndexed { i, dancer ->
            val newIndex = dancers.indexOf(dancer)
            mutations.add (newIndex)
        }


        (1..count).forEach {
            val dancerArray = dancers.toCharArray()
            mutations.forEachIndexed { i, mutation ->
                dancerArray.set(mutation, dancers.get(i))
            }
            dancers = dancerArray.joinToString("")
        }
        return dancers
    }

}