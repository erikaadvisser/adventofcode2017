package dec16

import util.FileInput

/**
 *
 */
fun main(args: Array<String>) {


//    val dance = Dance("abcde")
//    val input = Arrays.asList( "s1", "x3/4", "pe/b")
    val dance = Dance("abcdefghijklmnop")
    val input = FileInput("src/dec16/puzzle-16-1.txt").allTextAsList(",")

//    val result = dance.run(input)
//    println("result0a: ${dance.dancersStart}")
//    println("resu1t1a: $result")


    var start = System.currentTimeMillis()

    var result = dance.run(input)
    println("Result 1: $result")
    var count = 1
    while (result != dance.dancersStart) {
        result = dance.run(input)
        count ++
        if (count % 1000 == 0) {
            val end = System.currentTimeMillis()
            println("iteration $count in ${end - start} millis")
            start = end
        }
    }

    println("count $count")

    val actualToRun = (1_000_000_000 % count)

    (0 until actualToRun).forEach {
        dance.run(input)
    }

    println("result: ${dance.dancers}")



// input = 10_000 instructions = 10^5
// run = 1_000_000_000 times = 10^9
// total instructions = 10^14
// unrealistic minimum for each instruction = 10^3 clock cycles
// unrealistic minimum runtime = 10^17 clockcycles
// 1 ghz cpu = 10^9 cycles per second
// 10^8 seconds run time =


    // brute force:
    // 1000 iterations take 1887 millis, or 2 ms per go
    // 24 days to complete calculations ;)

//
//    val result2a = dance.run(input)
//    println("resu1t2a: $result2a")
//
//
//    val dance2 = Dance("abcdefghijklmnop")
//    val result1b = dance2.run(input)
//    println("result0b ${dance.dancersStart}")
//    println("resu1t1b: $result1b")
//    val result2b = dance2.runMutations(1)
//    println("resu1t2b: $result2b")
//
//

}

fun findLoop(dance: Dance, input: List<String>) {


}


