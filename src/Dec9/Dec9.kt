package Dec9

import dec8.Dec8
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.nio.charset.Charset
import java.util.*

fun main(args:Array<String>) {

    val lines = loadFile("2")
    val line = lines.first()

    val result = Dec9().run(line)

    println("result: $result")
}



private fun loadFile(id: String): List<String> {
    val fileName = "src/dec9/dec9-${id}.txt"
    return File(fileName).readLines(Charset.forName("US-ASCII"))
}


class Dec9 {

    fun run(line:String): Int {


        val reader = StringReader(line)
        reader.read() // opening {
        val groupReader = GroupReader(reader, 1)
        val rootGroup = groupReader.parse()
        return rootGroup.garbageChars()
    }


}

class GroupReader(
        private val reader:Reader,
        private val value:Int) {

    val currentGroup = Group(value)

    fun parse():Group {
        var c = nextChar()

        while (c != '}') {
            when(c) {
                '<' -> readGarbage()
                '{' -> readGroup()
                ',' -> ignore()
                else -> error("unexpected char $c")
            }
            c = nextChar()
        }
        return currentGroup
    }

    private fun ignore() {}

    fun nextChar():Char {
        var next:Int = reader.read()

        while (next.toChar() == '!') {
            reader.read()
            next = reader.read()
        }
        if (next == -1) {
            error("Unexpected end of string")
        }
        return next.toChar()
    }

    fun readGarbage() {
        var i = 0
        do {
            val next = nextChar()
            i ++
        }
        while (next != '>')

        val count = i -1 // offset for trailing '>'

        currentGroup.garbageCounts.add(count)
    }

    fun readGroup() {
        val subGroupReader = GroupReader(reader, value + 1)
        val subGroup = subGroupReader.parse()
        currentGroup.children.add(subGroup)
    }

}

class Group(
        private val value:Int) {

    val children = LinkedList<Group>()
    val garbageCounts = LinkedList<Int>()

    fun totalValue():Int {
        val childrenValue = children.fold(0) { total, it -> total + it.totalValue()}
        return childrenValue + value
    }

    fun garbageChars():Int {
        val childrenChars = children.fold(0) { total, it -> total + it.garbageChars()}
        return childrenChars + garbageCounts.sum()
    }

}