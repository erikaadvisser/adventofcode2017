package Dec9

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 *
 */
internal class Dec9Test {

    val toTest = Dec9()

    @Test
    fun `{}`() {
        val result = toTest.run("{}")
        assertEquals(1, result)
    }

    @Test
    fun `3 groups`() {
        val result = toTest.run("{{{}}}")
        assertEquals(6, result)
    }

    @Test
    fun `6 groups`() {
        val result = toTest.run("{{{},{},{{}}}}")
        assertEquals(16, result)
    }

    @Test
    fun `simple garbage`() {
        val result = toTest.run("{<a>,<a>,<a>,<a>}")
        assertEquals(1, result)
    }

    @Test
    fun `subgroup garbage garbage`() {
        val result = toTest.run("{{<a>},{<a>},{<a>},{<a>}}")
        assertEquals(9, result)
    }

    @Test
    fun `beautiful garbage`() {
        val result = toTest.run("{{<!>},{<!>},{<!>},{<a>}}")
        assertEquals(3, result)
    }

    @Test
    fun `strong garbage`() {
        val result = toTest.run("{{<!!>},{<!!>},{<!!>},{<!!>}}")
        assertEquals(9, result)
    }


    @Test
    fun `other garbage`() {
        val result = toTest.run("{{<a!>},{<a!>},{<a!>},{<ab>}}")
        assertEquals(3, result)
    }


}