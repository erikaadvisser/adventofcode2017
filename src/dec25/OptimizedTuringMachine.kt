package dec25

import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val time = measureTimeMillis { runOptimized() }
    println("time: $time millis")
}

fun runOptimized() {
//    val input = FileInput("src/dec24/puzzle-input.txt").toList()

    val optimized = OptimizedTuringMachine()
    optimized.run(12_134_527 )
    val checksum = optimized.checksum()

    println("part 1: $checksum")

}

class OptimizedTuringMachine {

    val tape = BooleanArray(1_000_000)
    var cursor = tape.size / 2
    var state = 'a'
    var current = false


    fun checksum(): Int {
        return tape.count { it }
    }

    fun run(steps: Int) {
        for (count in 1..steps) {

            when (state) {
/*      In state A:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state B.
        If the current value is 1:
        - Write the value 0.
        - Move one slot to the left.
        - Continue with state C. */
                'a' -> {
                    if (!current) {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'b'
                    } else {
                        tape[cursor] = false
                        cursor--
                        current = tape[cursor]
                        state = 'c'
                    }
                }

/*      In state B:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the left.
        - Continue with state A.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state C. */
                'b' -> {
                    if (!current) {
                        tape[cursor] = true
                        cursor--
                        current = tape[cursor]
                        state = 'a'
                    } else {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'c'
                    }
                }

/*      In state C:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state A.
        If the current value is 1:
        - Write the value 0.
        - Move one slot to the left.
        - Continue with state D.*/
                'c' -> {
                    if (!current) {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'a'
                    } else {
                        tape[cursor] = false
                        cursor--
                        current = tape[cursor]
                        state = 'd'
                    }
                }

/*      In state D:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the left.
        - Continue with state E.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the left.
        - Continue with state C.*/
                'd' -> {
                    if (!current) {
                        tape[cursor] = true
                        cursor--
                        current = tape[cursor]
                        state = 'e'
                    } else {
                        tape[cursor] = true
                        cursor--
                        current = tape[cursor]
                        state = 'c'
                    }
                }
/*    In state E:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state F.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state A.*/
                'e' -> {
                    if (!current) {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'f'
                    } else {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'a'
                    }
                }

/*      In state F:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state A.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state E.*/
                'f' -> {
                    if (!current) {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'a'
                    } else {
                        tape[cursor] = true
                        cursor++
                        current = tape[cursor]
                        state = 'e'
                    }
                }

            }

        }
    }


}
/*
- Write the value 1.
- Move one slot to the right.
7x

- Write the value 1.
- Move one slot to the left.
3x

- Write the value 0.
- Move one slot to the right.
0x

- Write the value 0.
- Move one slot to the left.
2x
*/