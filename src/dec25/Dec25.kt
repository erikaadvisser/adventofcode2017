package dec25

import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val time = measureTimeMillis { dec25.run() }
    println("time: $time millis")
}

fun run() {
//    val input = FileInput("src/dec24/puzzle-input.txt").toList()

    val machine = TuringMachine()
    machine.run(12_134_527 )
    val checksum = machine.checksum()

    println("part 1: $checksum")

}

class TuringMachine {

    val tape = BooleanArray(1_000_000)
    var cursor = tape.size / 2
    var state = 'a'
    var current = false

    fun write0Left() {
        tape[cursor] = false
        cursor --
        current = tape[cursor]
    }

    fun write0Right() {
        tape[cursor] = false
        cursor ++
        current = tape[cursor]
    }

    fun write1Left() {
        tape[cursor] = true
        cursor --
        current = tape[cursor]
    }

    fun write1Right() {
        tape[cursor] = true
        cursor ++
        current = tape[cursor]
    }

    fun run(steps: Int) {
        for (count in 1..steps) {
            runStep()
        }
    }

    fun checksum(): Int {
        return tape.count{ it }
    }

    fun runStep() {

        when (state) {
/*      In state A:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state B.
        If the current value is 1:
        - Write the value 0.
        - Move one slot to the left.
        - Continue with state C. */
            'a' -> {
                if (!current) {
                    write1Right()
                    state = 'b'
                    return
                }
                write0Left()
                state = 'c'
                return
            }

/*      In state B:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the left.
        - Continue with state A.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state C. */
            'b' -> {
                if (!current) {
                    write1Left()
                    state = 'a'
                    return
                }
                write1Right()
                state = 'c'
                return
            }

/*      In state C:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state A.
        If the current value is 1:
        - Write the value 0.
        - Move one slot to the left.
        - Continue with state D.*/
            'c' -> {
                if (!current) {
                    write1Right()
                    state = 'a'
                    return
                }
                write0Left()
                state = 'd'
                return
            }

/*      In state D:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the left.
        - Continue with state E.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the left.
        - Continue with state C.*/
            'd' -> {
                if (!current) {
                    write1Left()
                    state = 'e'
                    return
                }
                write1Left()
                state = 'c'
                return
            }
/*    In state E:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state F.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state A.*/
            'e' -> {
                if (!current) {
                    write1Right()
                    state = 'f'
                    return
                }
                write1Right()
                state = 'a'
                return
            }

/*      In state F:
        If the current value is 0:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state A.
        If the current value is 1:
        - Write the value 1.
        - Move one slot to the right.
        - Continue with state E.*/
            'f' -> {
                if (!current) {
                    write1Right()
                    state = 'a'
                    return
                }
                write1Right()
                state = 'e'
                return
            }

        }

    }

}
/*
- Write the value 1.
- Move one slot to the right.
7x

- Write the value 1.
- Move one slot to the left.
3x

- Write the value 0.
- Move one slot to the right.
0x

- Write the value 0.
- Move one slot to the left.
2x
*/