package dec14

import dec10.KnotHash

/**
 *
 */
class Defragmenter(val input: String) {

    private val rows = (0..127).map { hash(it) }

    fun usedSquares():Int {
        var count = 0
        rows.forEach { row ->
            val rowTotal = row.count { it == '1'}
            count += rowTotal
        }
        return count
    }


    fun regions(): Int {
        val unregioned = toPoints()

        var regionCount = 0
        while (unregioned.isNotEmpty()) {
            val region = findRegion(unregioned)
            regionCount ++
            unregioned.removeAll(region)
        }

        return regionCount
    }

    private fun findRegion(unregioned: MutableList<Point>): List<Point> {
        val start = unregioned.removeAt(0)
        val region = ArrayList<Point>()
        region.add(start)

        var connected = findConnected(region, unregioned)
        while (connected != null) {
            unregioned.remove(connected)
            region.add(connected)
            connected = findConnected(region, unregioned)
        }

        println("++ region added")
        return region
    }

    private fun findConnected(region: List<Point>, unregioned: List<Point>): Point? {
        region.forEach { regionCandidate ->
            val neighbour = unregioned.find { regionCandidate.neighbour(it) }
            if (neighbour != null) {
                println("added point")
                return neighbour
            }
        }
        return null
    }


    private fun toPoints(): MutableList<Point> {
        val points = ArrayList<Point>()

        rows.forEachIndexed { y, row ->
            row.forEachIndexed { x, c ->
                if (c == '1') {
                    points.add(Point(x, y))
                }
            }
        }

        return points
    }


    private fun hash(row: Int): String  {

        val transform = HashTransform()

        val hashInput = "$input-$row"
        val hasher = KnotHash(hashInput)
        val hash = hasher.run()
        val binary = transform.toBinary(hash)
        return binary
    }

}