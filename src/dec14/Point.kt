package dec14

/**
 *
 */
data class Point(val x: Int, val y: Int) {

    fun neighbour(other: Point): Boolean {
        val xdif = Math.abs(x - other.x)
        val ydif = Math.abs(y - other.y)
        return (xdif + ydif == 1)
    }
}