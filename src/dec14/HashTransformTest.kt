package dec14

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 *
 */
internal class HashTransformTest {

    val transform = HashTransform()

    @Test
    fun `00`() {
        assertEquals("00000000", transform.tupleToBinary("00") )
    }

    @Test
    fun `f0`() {
        assertEquals("11110000", transform.tupleToBinary("f0") )
    }

    @Test
    fun `0e`() {
        assertEquals("00101110", transform.tupleToBinary("2e") )
    }

    @Test
    fun `a0`() {
        assertEquals("10100000", transform.tupleToBinary("a0") )
    }


    @Test
    fun `a0c2017`() {
        assertEquals("00101110", transform.toBinary("a0c20170000000000000000000000000") )
    }


}