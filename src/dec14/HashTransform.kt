package dec14

/**
 *
 */
class HashTransform {

    fun toBinary(hash: String):String {
        val result = StringBuilder()

        (0..15).forEach {
            val part = hash.substring(it*2, 2 + it*2)
            val resultPart = tupleToBinary(part)
            result.append(resultPart)
        }

        return result.toString()
    }

    fun tupleToBinary(tuple: String): String {
        val asInteger = Integer.parseUnsignedInt(tuple, 16)

        val result = StringBuilder()
        (0..7).forEach {
            val mask = 1.shl(it)
            val test = asInteger.and(mask)
            if (test != 0 ) {
                result.append('1')
            }
            else {
                result.append('0')
            }
        }
        result.reverse()

        return result.toString()
    }

}