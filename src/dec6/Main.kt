package dec6

/**
 *
 */
fun main(args:Array<String>) {


    println ("${3159-1549}")
//    System.exit(1)

    val banks = ArrayList<Int>()
//    val input = "0\t2\t7\t0"
    val input = "2\t8\t8\t5\t4\t2\t3\t1\t5\t5\t1\t2\t15\t13\t5\t14"
    val inputParts = input.split("\t")
    inputParts.forEach{
        banks.add(Integer.parseInt(it))
    }

    val memory = Memory(banks)

    find(memory)

    val memory2 = Memory(memory.banks.banks)

    find(memory2)


// 1574
// 3157
}

fun find(memory: Memory) {
    memory.print()
    var oldState = memory.reallocate()
    var count = 1

    while (! oldState) {
        memory.print()
        oldState = memory.reallocate()
        count ++
    }
    memory.print()

    println("Count: $count")
}