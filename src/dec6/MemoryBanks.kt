package dec6

/**
 *
 */
data class MemoryBanks(val banks: ArrayList<Int>) {

    fun reallocate() {
        val highest = banks.max()!!
        val highestIndex = banks.indexOf(highest)

        banks[highestIndex] = 0

        var current = nextIndex(highestIndex)

        (1..highest).forEach {
            banks[current] ++
            current = nextIndex(current)
        }
    }

    fun nextIndex(index:Int):Int {
        return (index + 1) % banks.size
    }

    fun print() {
        banks.forEach{print("$it, ")}
    }
}