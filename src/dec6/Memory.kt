package dec6

/**
 *
 */
class Memory(val input: ArrayList<Int>) {

    val banks = MemoryBanks(input)
    val configurations = HashSet<MemoryBanks>()

    init {
        configurations.add(banks)
    }

    fun reallocate():Boolean {
        banks.reallocate()
        val existingConfiguration = configurations.contains(banks)
        configurations.add(banks)
        return existingConfiguration
    }

    fun print() {
        banks.print()
        println();
    }
}