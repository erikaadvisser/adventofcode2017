package dec13

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.File

/**
 *
 */
internal class Dec13Test {

    @Test
    fun exampleTest() {
        val file = File("src/dec13/example-input.txt")
        val input = file.readLines()

        val (severity, delay) = Dec13(input).run()

        println("delay: $delay")



        assertEquals(24, severity)

    }
}