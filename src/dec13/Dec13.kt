package dec13

import java.io.File
import java.util.*

fun main(args: Array<String>) {

    val file = File("src/dec13/puzzle-input.txt")
    val input = file.readLines()

    val (severity, part2) = Dec13(input).run()
    println("severity: $severity")
    println("delay: $part2")
}

data class Result(val severity: Int, val delay: Int)

class Dec13(lines: List<String>) {

    val fireWalls = createFirewalls(lines)
    val delayedFirewalls = createFirewalls(lines)

    fun run():Result {
        var severity = severityCrossWithoutDelay()
        fireWalls.values.forEach { it.reset() }

        var delay = 0
        var caught = true
        while( caught) {
            println("Delay: $delay - Caught: $caught")
            delay ++
            caught = crossWithDelay()
        }
        return Result(severity,delay)
    }

    fun severityCrossWithoutDelay():Int {
        cross()
        val severity = fireWalls.values.fold(0) { total, it -> total + it.severity()}
        return severity
    }

    fun crossWithDelay():Boolean {
        delayedFirewalls.values.forEach{ it.scan() }

        fireWalls.clear()
        delayedFirewalls.values.forEach {
            val copy = it.copy()
            fireWalls.put(copy.depth, copy)
        }

        cross()
        val caught = fireWalls.values.fold( false ) { caught, it -> caught || it.caught }

        return caught
    }

    fun cross() {
        val maxDepth = fireWalls.keys.max() ?: 0

        (0..maxDepth).forEach { currentDepth ->
            fireWalls[currentDepth]?.moveInto()
            fireWalls.values.forEach{ it.scan() }
        }
    }

    private fun createFirewalls(lines: List<String>): MutableMap<Int, Firewall> {
        val fireWalls = HashMap<Int, Firewall>()

        lines.forEach {
            val scanner = Scanner(it).useDelimiter(": ")
            val depth = scanner.nextInt()
            val range = scanner.nextInt()

            val fireWall = Firewall(depth, range)
            fireWalls.put(depth, fireWall)
//            println ("firewall layer: $depth - depth: $range")
        }

        return fireWalls
    }
}

data class Firewall(val depth:Int,
                    val range:Int) {

    private var scanDepth = 0
    var caught = false
    private var directionDown = true

    fun copy():Firewall {
        val copy = Firewall(depth, range)
        copy.scanDepth = this.scanDepth
        copy.directionDown = this.directionDown
        return copy
    }

    fun moveInto() {
        if (scanDepth == 0) {
            caught = true
        }
    }

    fun scan() {
        if (directionDown) {
            scanDepth ++
        }
        else {
            scanDepth --
        }
        if (scanDepth == 0 || scanDepth == range - 1) {
            directionDown = ! directionDown
        }
    }

    fun severity():Int {
        if (caught) {
            val result = depth * range
            return result
        }
        return 0
    }

    fun reset() {
        scanDepth = 0
        directionDown = true
        caught = false
    }
}