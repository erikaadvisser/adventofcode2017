package dec19

/**
 *
 */
data class MazePoint(val x: Int, val y: Int, val c: Char) {

    fun neighbour(other: MazePoint, deltaX: Int, deltaY: Int): Boolean {
        return (x + deltaX == other.x && y + deltaY == other.y)
    }
}