package dec19

import util.FileInput


fun main(args: Array<String>) {

    val lines = FileInput("src/dec19/19-1.txt").toList()
//    val lines = FileInput("src/dec19/19-sample.txt").toList()

    val walker = Walker(lines)
    val letters = walker.runMaze()
    println("Letters: $letters, stepcount: ${walker.stepCount}")
}

class Walker(lines: List<String>) {

    val points = createPoints(lines)
    var current: MazePoint? = findStart()
    var direction = 's'
    val letters = StringBuilder()
    var stepCount = 0

    fun runMaze(): String {
        do {
            stepCount ++
            addCurrentIfLetter(current)
            current = findNext()
        } while (current != null)

        return letters.toString()
    }

    private fun findNext(): MazePoint? {
        val s = points.find{ current!!.neighbour(it, 0, 1) }
        val n = points.find{ current!!.neighbour(it, 0, -1) }
        val e = points.find{ current!!.neighbour(it, 1, 0) }
        val w = points.find{ current!!.neighbour(it, -1, 0) }

        when(direction) {
            's' -> return walk(s, e, 'e', w, 'w')
            'n' -> return walk(n, w, 'w', e, 'e')
            'e' -> return walk(e, n, 'n', s, 's')
            'w' -> return walk(w, s, 's', n, 'n')
            else -> error("unknown direction $direction")
        }

    }

    private fun walk(ahead: MazePoint?, left: MazePoint?, leftName: Char, right: MazePoint?, rightName: Char): MazePoint? {
        if ( ahead != null ) {
            return ahead
        }
        if ( left != null ) {
            direction = leftName
            return left
        }
        if ( right != null ) {
            direction = rightName
            return right
        }
        return null
    }

    fun addCurrentIfLetter(mazePoint: MazePoint?) {
        if (mazePoint != null) {
            val c = mazePoint.c
            if (c != '|' && c != '-' && c != '+') {
                letters.append(c)
            }
        }
    }

    private fun findStart(): MazePoint {
        return points.minBy { it.y }!!
    }


    private fun createPoints(lines: List<String>): MutableList<MazePoint> {
        val points = ArrayList<MazePoint>()

        lines.forEachIndexed { y, row ->
            row.forEachIndexed { x, c ->
                if (c != ' ') {
                    val point = MazePoint(x, y, c)
                    points.add(point)
                }
            }
        }

        return points
    }
}

