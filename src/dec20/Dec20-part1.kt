package dec20

import util.FileInput
import java.lang.Math.abs
import java.util.*

/**
 *
 */
fun main(args: Array<String>) {
    val input = FileInput("src/dec20/20.txt").toList()


    val accelerationsSums = input.map { toAccelerationSums(it) }

    val min = accelerationsSums.minBy { sumVector(it) }

    val minIndex = accelerationsSums.indexOf(min)

    println ("min: $minIndex")
}

fun toAccelerationSums(line: String): String {
    val lastPart = line.split(("a=<"))[1]

    val acceleration = lastPart.substring(0, lastPart.length - 1)

    return acceleration
}

fun sumVector(input: String): Int {
    var total = 0
    val scanner = Scanner(input).useDelimiter(",")
    total += abs(scanner.nextInt())
    total += abs(scanner.nextInt())
    total += abs(scanner.nextInt())
    return total
}