package dec20

import util.FileInput
import java.util.*

/**
 *
 */
fun main(args: Array<String>) {
    val input = FileInput("src/dec20/20.txt").toList()

    val particles = ArrayList<Particle>( input.map{ toParticle(it) } )


    var count = 0
    while (true) {
        val collisions = calcColissions(particles)
        particles.removeAll(collisions)
        particles.forEach {  it.tick() }
        println("iteration: $count, particles left: ${particles.size}")
        count ++
    }



}

fun calcColissions(particles: List<Particle>): List<Particle> {
    val possibleColliders = ArrayList<Particle> (particles)

    val colliders = ArrayList<Particle>()

    particles.forEach { candidate ->
        possibleColliders.remove(candidate)
        val collidesWithCandidate = possibleColliders.filter{ candidate.hit(it) }
        if (collidesWithCandidate.isNotEmpty()) {
            colliders.addAll(collidesWithCandidate)
            colliders.add(candidate)
            possibleColliders.removeAll(collidesWithCandidate)
        }
    }

    return colliders
}


fun toParticle(line: String): Particle {
    val scanner = Scanner(line).useDelimiter(", ")
    val locationPart = scanner.next()
    val speedPart = scanner.next()
    val accelerationPart = scanner.next()

    val location = toVector(locationPart)
    val speed = toVector(speedPart)
    val acceleration = toVector(accelerationPart)

    return Particle(location, speed, acceleration)
}

fun toVector(input: String): Vector {
    val content = input.substring(3, input.length - 1)
    val scanner = Scanner(content).useDelimiter(",")
    val x = scanner.nextLong()
    val y = scanner.nextLong()
    val z = scanner.nextLong()
    return dec20.Vector(x, y, z)
}

inline operator fun Vector.plus(other: Vector): Vector = this.add(other)


data class Vector(val x: Long, val y: Long, val z: Long) {

    fun add(other: Vector): Vector {
        return Vector (x + other.x, y + other.y, z + other.z)
    }

}

data class Particle(var location: Vector, var speed: Vector, val acceleration: Vector) {

    fun tick() {
        speed += acceleration
        location += speed
    }

    fun hit(other: Particle): Boolean {
        return location == other.location
    }
}