package dec12

import java.io.File

/**
 *
 */
fun main(args: Array<String>) {

    val file = File("src/dec12/puzzle-input.txt")
    val input = file.readLines()

    val (zero, total) = Dec12(input).run()
    println("Members of group with zero: $zero")
    println("groups: $total")
}

data class Result(val groupZeroSize:Int, val groupCount:Int)

class Dec12(private val lines:List<String>) {

    val groups = ArrayList<MutableSet<Int>>()


    fun run():Result {
        lines.forEach { processLine(it) }

        val groupWithZero = groups.find { it.contains( 0) } !!
        return Result(groupWithZero.size, groups.size)
    }

    private fun processLine(line: String) {
        val program = parseProgram(line)
        val connectedPrograms = parseConnections(line)
        connectedPrograms.add( program )

        val existingGroups = HashSet<MutableSet<Int>>()

        connectedPrograms.forEach { program ->
            val partOfGroups = groups.filter { it.contains( program ) }
            existingGroups.addAll( partOfGroups )
        }

        if (existingGroups.isEmpty()) {
            groups.add ( connectedPrograms )
        }
        else {
            val mergedGroup = mergeGroups(existingGroups)
            mergedGroup.addAll( connectedPrograms )
        }
    }

    private fun parseConnections(line: String): MutableSet<Int> {
        val normalized = line.replace(",", "")
        val linkParts = normalized.split("<-> ")
        val parts = linkParts[1].split(" ")

//        return parts.fold( HashSet<Int>() ) { set, it -> set.add ( Integer.parseInt( it ) ); set }
        val result = HashSet<Int>()
        parts.forEach { result.add ( Integer.parseInt( it ) ) }
        return result
    }

    private fun parseProgram(line: String): Int {
        val parts = line.split(" ")
        return Integer.parseInt(parts[0])
    }

    private fun mergeGroups(existingGroups: Set<MutableSet<Int>>): MutableSet<Int> {
        groups.removeAll( existingGroups )
        val merged = HashSet<Int>()

        existingGroups.forEach {
            merged.addAll(it)
        }

        groups.add(merged)
        return merged
    }


}