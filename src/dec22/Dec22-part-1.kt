package dec22

import util.FileInput


fun main(args: Array<String>) {
    val input = FileInput("src/dec22/22-1.txt").toList()

    val virus = Virus(input)

    (1..10000).forEach {
        virus.burst()
    }

    println("infect count = ${virus.infectCount}")
}


class Virus(input: List<String>) {

    val extraSize = 1000

    val xSize = input[0].length + extraSize
    val ySize = input.size + extraSize
    val grid = parseInput(input)

    var x = (xSize - 1) / 2
    var y = (ySize - 1) / 2
    var vx = 0
    var vy = -1

    var moveCount = 0
    var infectCount = 0

    private fun parseInput(input: List<String>): Array<CharArray> {
        val grid = Array(ySize, { CharArray(xSize, {'.'}) })

        val offset = extraSize / 2

        input.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                grid[y + offset][x + offset] = c
            }
        }

        return grid
    }

    fun burst() {
        changeDirectionAndFlip()
        move()
    }

    private fun changeDirectionAndFlip() {
        val current = grid[y][x]
        when( current ) {
            '#' -> atInfected()
            '.' -> atClean()
            else -> error("invalid input at $x, $y: '$current'")
        }
    }

    fun atClean() {
        grid[y][x] = '#'
        val oldVx = vx
        vx = vy
        vy = 0 - oldVx
        infectCount ++
    }

    fun atInfected() {
        grid[y][x] = '.'
        val oldVx = vx
        vx = 0 - vy
        vy = oldVx
    }


    fun move() {
        x += vx
        y += vy
        moveCount ++

        if (x < 0 || x >= xSize) {
            error("out of bounds: x = $x after $moveCount moves")
        }
        if (y < 0 || y >= ySize) {
            error("out of bounds: y = $y after $moveCount moves")
        }
    }

}