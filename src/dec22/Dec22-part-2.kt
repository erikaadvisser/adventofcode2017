package dec22

import util.FileInput
import kotlin.system.measureTimeMillis


fun main(args: Array<String>) {
    val time = measureTimeMillis { run() }
    println("time: $time millis")
}

fun run() {
    val input = FileInput("src/dec22/22-1.txt").toList()

    val virus = VirusEvolved(input)

    (1..10000000).forEach {
        virus.burst()
    }

    println("infect count = ${virus.infectCount}")

}


class VirusEvolved(input: List<String>) {

    val extraSize = 1000

    val xSize = input[0].length + extraSize
    val ySize = input.size + extraSize
    val grid = parseInput(input)

    var x = (xSize - 1) / 2
    var y = (ySize - 1) / 2
    var vx = 0
    var vy = -1

    var moveCount = 0
    var infectCount = 0

    private fun parseInput(input: List<String>): Array<CharArray> {
        val grid = Array(ySize, { CharArray(xSize, {'.'}) })

        val offset = extraSize / 2

        input.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                grid[y + offset][x + offset] = c
            }
        }

        return grid
    }

    fun burst() {
        changeDirectionAndFlip()
        move()
    }

    private fun changeDirectionAndFlip() {
        val current = grid[y][x]
        when( current ) {
            '#' -> atInfected()
            '.' -> atClean()
            'W' -> atWeakened()
            'F' -> atFlagged()
            else -> error("invalid input at $x, $y: '$current'")
        }
    }

    private fun atFlagged() {
        grid[y][x] = '.'
        vx = 0 - vx
        vy = 0 - vy
    }

    private fun atWeakened() {
        grid[y][x] = '#'
        infectCount ++
    }

    fun atClean() {
        grid[y][x] = 'W'
        val oldVx = vx
        vx = vy
        vy = 0 - oldVx
    }

    fun atInfected() {
        grid[y][x] = 'F'
        val oldVx = vx
        vx = 0 - vy
        vy = oldVx
    }


    fun move() {
        x += vx
        y += vy
        moveCount ++

        if (x < 0 || x >= xSize) {
            error("out of bounds: x = $x after $moveCount moves")
        }
        if (y < 0 || y >= ySize) {
            error("out of bounds: y = $y after $moveCount moves")
        }
    }

}