package dec18

import util.FileInput
import java.math.BigInteger
import java.util.*

fun main(args: Array<String>) {

    val lines = FileInput("src/dec18/18-1.txt").toList()

    val duet = Duet(lines)
    duet.play()
}

class Duet(val instructions:List<String>) {

    val registers = HashMap<String, BigInteger>()
    var sound = BigInteger.ZERO
    var instructionIndex = 0

    fun play() {
       while(true) {
           if (instructionIndex < 0 || instructionIndex >= instructions.size) {
               println("abort - current $instructionIndex")
               exit()
           }
           val line = instructions[instructionIndex]
           val scanner = Scanner(line)
           val command = scanner.next()
           val arg1 = scanner.next()
           val arg2 = if (scanner.hasNext()) scanner.next() else ""

           when(command) {
               "set" -> set(arg1, arg2)
               "add" -> add(arg1, arg2)
               "mul" -> mul(arg1, arg2)
               "mod" -> mod(arg1, arg2)
               "jgz" -> jgz(arg1, arg2)
               "snd" -> snd(arg1)
               "rcv" -> rcv(arg1)
               else -> TODO("unimplemented: $command")
           }

           instructionIndex ++

       }
    }

    private fun add(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current + parseArg2(arg2)
        set(arg1, new)
    }

    private fun mul(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current * parseArg2(arg2)
        set(arg1, new)
    }

    private fun mod(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        val new = current % parseArg2(arg2)
        set(arg1, new)
    }

    private fun jgz(arg1: String, arg2: String) {
        val current = registers.getOrDefault(arg1, BigInteger.ZERO)
        if (current > BigInteger.ZERO) {
            instructionIndex --
            instructionIndex += parseArg2(arg2).toInt()
        }
    }


    private fun snd(arg1: String) {
        sound = parseArg2(arg1)
    }

    private fun rcv(arg1: String) {
        if (parseArg2(arg1) !=  BigInteger.ZERO) {
            println("recoverd: $sound")
            exit()
        }
    }


    fun set(arg1: String, arg2: String) {
        val value = parseArg2(arg2)
        set(arg1, value)
    }

    fun parseArg2(arg2: String):BigInteger {
        try {
            val value = BigInteger(arg2)
            return value
        }
        catch (e: NumberFormatException) {
            return registers.getOrPut(arg2, {BigInteger.ZERO})
        }
    }

    fun set(arg1: String, value: BigInteger) {
        registers.put(arg1, value)
    }


    private fun exit() {
        System.exit(0)
    }
}

