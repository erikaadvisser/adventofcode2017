package dec18

import util.FileInput
import java.util.*

fun main(args: Array<String>) {

    val lines = FileInput("src/dec18/18-1.txt").toList()
    val instructions = lines.map { it -> Instruction(it)}

    val program0 = Program(instructions, 0)
    val program1 = Program(instructions, 1)

    program0.other = program1
    program1.other = program0

    val start = System.currentTimeMillis()
    var mid = start

    var count = 0L
    while (true) {
//    while (count < 1500) {
        count ++
        print("$count: ")
        if (count % 10_000_000L == 0L) {
            val now = System.currentTimeMillis()
            println(" busy ($count): ${(now - mid) / 1000} total: ${(now - start) / 1000} average = ${1000 * count / (now-start)}/s \t\tp0.queue = ${program0.queue.size}\tp1.queue = ${program1.queue.size}")
            mid = now
        }
        if ((program0.waiting || program0.terminated) && (program1.waiting || program1.terminated)) {
            println("program 0 waiting: ${program0.waiting} terminated: ${program0.terminated} total: ${program0.count}")
            println("program 1 waiting: ${program1.waiting} terminated: ${program1.terminated} total: ${program1.count}")
            return
        }

        if (program0.queue.size == 0 && program1.queue.size == 0) {

            val state0 = program0.runOnce()
            val state1 = program1.runOnce()
//            println("$state0\t**\t$state1")
            assertit(program0, program1, "both=0")
        }
        else {
            if (program0.queue.size > 0) {
                val state0 = program0.runOnce()
//                println("$state0")
                assertit(program0, program1, "0qs>0")
            }
            else {
                val state1 = program1.runOnce()
//                println("$\t\t\t\t\t\t\t\t\t\t**\t$state1")
                assertit(program0, program1, "0qs<=0")
            }
        }
    }
}

fun assertit(program0: Program, program1: Program, where: String) {
    if (program0.queue.size > 1) {
        println("queue error: $where")
        System.exit(1)
    }
}


fun s15(value: Long?): String {
    return String.format("%-15d", value?: 0L)
}

fun s13(value: Long?): String {
    return String.format("%-13d", value?: 0L)
}

fun s6(value: Long?): String {
    return String.format("%-6d", value?: 0L)
}

fun i5(value: Int): String {
    return String.format("%-5d", value)
}

fun t(value: String): String {
    return String.format("%-6s", value)
}


class Program(private val instructions: List<Instruction>, private val programNumber: Int) {

    private val registers = HashMap<String, Long>()
    private var instructionIndex = 0
    var waiting = false
    var terminated = false
    var count = 0

    var other: Program? = null

    var queue = LinkedList<Long>()

    init {
        registers["a"] = 0L
        registers["b"] = 0L
        registers["f"] = 0L
        registers["i"] = 0L
        registers["p"] = programNumber.toLong()
    }


    fun runOnce():String {
        waiting = false
        if (terminated) {
            return "terminated"
        }


        if (instructionIndex < 0 || instructionIndex >= instructions.size) {
            println("abort - current $instructionIndex")
            terminated = true
        }
        val instruction = instructions[instructionIndex]

        val before = stateToString(instruction)


        when (instruction.command) {
            "set" -> set(instruction)
            "add" -> add(instruction)
            "mul" -> mul(instruction)
            "mod" -> mod(instruction)
            "jgz" -> jgz(instruction)
            "snd" -> snd(instruction)
            "rcv" -> rcv(instruction)
            else -> error("unimplemented: $instruction.command")
        }

        instructionIndex++


        return before
    }

    private fun stateToString(instruction: Instruction): String {
        val state = StringBuilder()
        state.append("p$programNumber[$instructionIndex] $instruction || ")
        state.append("p: ${s15(registers["p"])}")
        state.append("a: ${s13(registers["a"])}")
        state.append("b: ${s13(registers["b"])}")
        state.append("f: ${s6(registers["f"])}")
        state.append("i: ${s6(registers["i"])}")
        state.append("q=${i5(queue.size)}")
        return state.toString()
    }

    private fun add(instruction: Instruction) {
        val current = registers.get(instruction.arg1)!!
        val new = current + instruction.parseArg2(registers)
        set(instruction.arg1, new)
    }

    private fun mul(instruction: Instruction) {
        val current = registers.get(instruction.arg1)!!
        val new = current * instruction.parseArg2(registers)
        set(instruction.arg1, new)
    }

    private fun mod(instruction: Instruction) {
        val current = registers.get(instruction.arg1)!!
        val new = current % instruction.parseArg2(registers)
        set(instruction.arg1, new)
    }

    private fun jgz(instruction: Instruction) {
        val evaluate = instruction.parseArg1(registers)
        if (evaluate > 0L) {
            instructionIndex--
            instructionIndex += instruction.parseArg2(registers).toInt()
        }
    }


    private fun snd(instruction: Instruction) {
        other!!.acceptSend(instruction.parseArg1(registers))
        count++
    }

    private fun acceptSend(value: Long) {
        queue.add(value)
    }

    private fun rcv(instruction: Instruction) {
        if (queue.isEmpty()) {
            instructionIndex--
            waiting = true
        } else {
            val received = queue.removeFirst()
            set(instruction.arg1, received)
        }
    }

    private fun set(instruction: Instruction) {
        val value = instruction.parseArg2(registers)
        set(instruction.arg1, value)
    }

    private fun set(arg1: String, value: Long) {
        registers.put(arg1, value)
    }


}



data class Instruction(val input: String) {

    val command = parseCommand(input)
    val arg1 = parseArg1(input)
    val arg2 = parseArg2(input)

    val arg1BigInt = parseBigInt(arg1)
    val arg2BigInt = parseBigInt(arg2)


    private fun parseBigInt(arg: String): Long? {
//        if ()
        try {
            val value = arg.toLong()
            return value
        } catch (e: NumberFormatException) {
            return null
        }
    }

    fun parseArg1(registers: HashMap<String, Long>): Long {
        if (arg1BigInt != null) {
            return arg1BigInt
        }
        else {
            return registers.getOrPut(arg1, { 0L })
        }
    }

    fun parseArg2(registers: HashMap<String, Long>): Long {
        if (arg2BigInt != null) {
            return arg2BigInt
        }
        else {
            return registers.getOrPut(arg2, { 0L })
        }
    }



    private fun parseCommand(input: String): String {
        val scanner = Scanner(input)
        return scanner.next()
    }


    private fun parseArg1(input: String): String {
        val scanner = Scanner(input)
        scanner.next()
        return scanner.next()
    }


    private fun parseArg2(input: String): String {
        val scanner = Scanner(input)
        scanner.next()
        scanner.next()
        if (scanner.hasNext()) {
            return scanner.next()
        }
        return ""
    }

    override fun toString(): String {
        return "$command $arg1 ${t(arg2)}"
    }
}
