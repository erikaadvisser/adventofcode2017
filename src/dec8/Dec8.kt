package dec8

import java.io.File
import java.nio.charset.Charset
import java.util.*
import kotlin.collections.HashMap


fun main(args:Array<String>) {
    val result = Dec8().run("1")

    println("max: $result")
}

class Dec8 {

    fun run(id:String):Int {
        val lines = loadFile(id)

        val operations = lines.map {
            parseToInstruction(it)
        }

        val registers = HashMap<String, Int>()

        val stepMaxes = LinkedList<Int>()

        operations.forEach {
            it.execute(registers)
            stepMaxes.add(registers.values.max() ?: 0)
        }

//        star 1
//        val max = registers.values.max()!!

//        star 2
        val max = stepMaxes.max()!!
        return max
    }


    private fun loadFile(id: String): List<String> {
        val fullName = "src/dec8/dec8-${id}.txt"
        val normalizedLines = ArrayList<String>()

        File(fullName).readLines(Charset.forName("US-ASCII")).forEach {
            val normalized = it.replace(", ", " ")
            normalizedLines.add(normalized)
        }

        return normalizedLines
    }


    private fun parseToInstruction(it: String): Instruction {
        val scanner = Scanner(it)

        val operationRegister = scanner.next()
        val operationName = scanner.next()
        val operationAmount = scanner.nextInt()
        scanner.next() // if
        val conditionRegister = scanner.next()
        val condition = scanner.next()
        val conditionValue = scanner.nextInt()

        val operationIncrement = parseOperation(operationName)

        val conditionFunction = parseCondition(condition)

        return Instruction(operationRegister, operationAmount, operationIncrement,
                conditionValue, conditionRegister, conditionFunction)
    }


    private fun lesser(left: Int, right: Int):Boolean = left < right
    private fun greater(left: Int, right: Int):Boolean = left > right
    private fun lesserOrEquals(left: Int, right: Int):Boolean = left <= right
    private fun greaterOrEquals(left: Int, right: Int):Boolean = left >= right
    private fun equal(left: Int, right: Int):Boolean = left == right
    private fun notEqual(left: Int, right: Int):Boolean = left != right


    private fun parseCondition(condition: String): (left: Int, right: Int) -> Boolean {

        when(condition) {
            "<" -> return ::lesser
            ">" -> return ::greater
            "<=" -> return ::lesserOrEquals
            ">=" -> return ::greaterOrEquals
            "==" -> return ::equal
            "!=" -> return ::notEqual
            else -> throw IllegalArgumentException("unsupported condition '$condition'")
        }
    }

    private fun parseOperation(operationName: String): Boolean {
        return "inc" == operationName
    }


}

class Instruction(
        val operationRegister:String,
        val amount:Int,
        val increment:Boolean,
        val conditionValue:Int,
        val conditionRegister:String,
        val conditionOperation: (left:Int, right:Int) -> Boolean) {

    fun execute(registers:MutableMap<String, Int>) {

        val conditionRegisterValue:Int = registers[conditionRegister] ?: 0
        val conditionSatisfied = conditionOperation(conditionRegisterValue, conditionValue)

        if (conditionSatisfied) {
            val sign:Int
            if (increment) {sign = 1}
            else { sign = -1}

            val finalAmount = sign * amount

            val currentValue:Int = registers.getOrPut(operationRegister, {0})
            registers[operationRegister] = currentValue + finalAmount
        }
    }

}
