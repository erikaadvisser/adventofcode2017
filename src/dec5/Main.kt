package dec5

import java.io.File
import java.util.*

fun main(args:Array<String>) {


    val input = LinkedList<Int>()
    File("src/dec5/dec5-1.txt").readLines(charset("US-ASCII")).forEach(){ it: String ->
        input.add(Integer.parseInt(it))
    }


    val computer = Computer(input)

    var count = 0
    while(true) {
        count++
        if (computer.execute()) {
            break
        }
    }

    println("Executions: $count")

}