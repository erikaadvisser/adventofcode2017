package dec5

import java.util.*

class Computer(val memory:LinkedList<Int>) {

    private var instructionIndex = 0

    fun execute():Boolean {

        val jumpCount = memory[instructionIndex]

        if (jumpCount >= 3) {
            memory[instructionIndex] --
        }
        else {
            memory[instructionIndex] ++
        }

        instructionIndex += jumpCount

        return (instructionIndex >= memory.size)
    }

}