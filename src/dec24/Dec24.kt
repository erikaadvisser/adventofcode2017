package dec24

import sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl.ThreadStateMap.Byte1.other
import util.FileInput
import kotlin.system.measureTimeMillis

/**
 *
 */

fun main(args: Array<String>) {
    val time = measureTimeMillis { dec24.run() }
    println("time: $time millis")
}

fun run() {
    val input = FileInput("src/dec24/puzzle-input.txt").toList()

    val bridgeBuilder = BridgeBuilder(input)
    bridgeBuilder.buildOptions()

    val maxStrength = bridgeBuilder.maxStrength()
    val longestStrength = bridgeBuilder.longestStrength()


    println("Max strength: $maxStrength")
    println("Longest strength: $longestStrength")

}

class BridgeBuilder(input: List<String>) {
    val components = input.map {
        val values = it.split("/")
        Component(values[0].toInt(), values[1].toInt())
    }

    val options = HashMap<List<Component>, Int>()

    fun buildOptions() {
        build(0, components, ArrayList())
    }

    fun maxStrength(): Int {
        val maxStrength = options.values.max() ?: 0
        return maxStrength
    }

    fun longestStrength(): Int {
        val maxLength = options.keys.maxBy { it.size }?.size ?: 0
        val longest = options.filter { it.key.size == maxLength }

        val maxStrength = longest.values.max() ?: 0
        return maxStrength
    }

    fun build(pins: Int, componentsLeft: List<Component>, componentsUsed: List<Component>) {

        val nextComponents = componentsLeft.filter { it.connects(pins) }
        nextComponents.forEach { componentToAdd ->
            val newComponentsLeft = ArrayList(componentsLeft)
            val newComponentsUsed = ArrayList(componentsUsed)
            newComponentsLeft. remove(componentToAdd)
            newComponentsUsed.add(componentToAdd)
            val newPins = componentToAdd.otherPins(pins)

            build(newPins, newComponentsLeft, newComponentsUsed)
        }

        val strength = componentsUsed.fold(0) { total, it -> total + it.strength() }
        options.put(componentsUsed, strength)
    }

    fun printOptions() {
        options.forEach { option ->
            print(" %4d - ".format(option.value))
            option.key.forEach { component ->
                print("${component.start}/${component.end}--")
            }
            println()
        }
    }
}

data class Component(val start: Int, val end: Int) {

    fun connects(pins: Int): Boolean {
        return (this.start == pins || this.end == pins )
    }

    fun strength(): Int {
        return start + end
    }

    fun otherPins(pins: Int): Int {
        if (pins == start) { return end }
        return start
    }
}